QuickStarts Go

objective
The objective of creating this web service in golang for a QuickStart is that people who seek to learn a powerful language such as golang can start with something that is widely used such as web services.

Description
Web service in golang using sqlite as database. A note table is used to handle post, get, put and delete from the go web services.

Folders
* Videos:
     * Unit 1: Installation of golang and first steps in the language.
     * Unit 2: Creation of web services without a database.
     * Unit 3: Installation of mingw, sqlite, integration in web services and test in telephone application made in flutter.
* Src:
     * Source code of the web service.

Creator
* Name: Walker Manrique Chalco
* Email: wmanriquec@gmail.com

Modify bracnh GO_en
* Name: Fabricio Centeno Cárdenas
* Email: rcentenoc@ulasalle.edu.pe